const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const Dotenv = require("dotenv-webpack");
//const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  devtool: "inline-source-map",
  devServer: {
    contentBase: "./dist",
    port: 8081
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new HtmlWebpackPlugin({
      title: "Development",
      template: "./my-index.ejs"
    }),
    new Dotenv()
    //    new BundleAnalyzerPlugin()
  ],
  module: {
    rules: [
      /*
      {
        test: require.resolve("maximoplus-core"),
        use: "imports-loader?this=>window"
      },*/
      {
        test: /\.js$/,
        exclude: [/node_modules/, /maximoplus-core/],
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.js$/,
        use: {
          loader: "source-map-loader"
        }
      },
      /*      {
        test: /\.js$/,
        exclude: [/maximoplus-core/],
        loader: "source-map-loader",
        enforce: "pre"
      },*/
      {
        test: /\.css$/,
        exclude: /typeface-roboto/,
        use: ["to-string-loader", "css-loader"]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
        include: /typeface-roboto/
      },
      {
        test: /\.scss$/,
        use: [
          { loader: "to-string-loader" },
          { loader: "css-loader" },
          { loader: "sass-loader" }
        ]
      },
      /*
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      }*/

      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: {
          loader: "url-loader?limit=1000&mimetype=application/font-woff",
          options: {
            outputPath: "images"
          }
        }
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: {
          loader: "file-loader",
          options: {
            outputPath: "images"
          }
        }
      }
    ]
  }
};
