import "maximoplus-core";
import "@webcomponents/webcomponentsjs/custom-elements-es5-adapter.js";

import { FilterDialog as WFilterDialog } from "mplus-webcomponents";

//WebComponents are defined inside each of these packages, we need to import them for side effects only
import "./components/Icons.js";
import "./components/InfoCircle.js";
import "./components/SidenavAction.js";
import "./components/MatAppBar.js";
import "./components/MatLayout.js";
import "./components/AppPages.js";
import "./components/DialogHolder.js";
import "./components/LookupField.js";
import "./components/TextField.js";
import "./components/Section.js";
import "./components/List.js";
import "./components/LoginForm.js";
import "./components/ListDialog.js";
import "./components/Alert.js";
import "./components/RadioButton.js";
import "./components/WorkflowDialog.js";
import "./components/ErrorAlert.js";

import { MPlusComponent as WMPlusComponent } from "mplus-webcomponents";
import { getRootEl } from "./utils.js";

import sidenavstyles from "sidenavjs/dist/sidenav.css";
//using just css loader, not the style loaders, because we have to insert the style into the web components, it will not be visible from the outside

import defineMuiComponents from "muicss-webcomp";

import themes from "./css/theme-default.css";

defineMuiComponents();

customElements.define("filter-dialog", WFilterDialog);

maximoplus.net.globalFunctions.serverRoot = () => process.env.SERVER_ROOT;

maximoplus.core.globalFunctions.handleErrorMessage = error => {
  getRootEl().openErrorDialog(error);
};

WMPlusComponent.prototype.pushDialog = function(dialog) {
  let el = getRootEl();
  el.pushDialog(dialog);
};

WMPlusComponent.prototype.popDialog = function() {
  let el = getRootEl();
  el.removeDialog();
};

//TODO check maxcomponents.json - sideNavActions is of the type array with the basic type action. Type action will be the special type, that will be defined in the types.json, but the value list of actins will be defined inside the maximoplus-gui

window["actions"] = {
  action3: ev => {
    console.log("Global action action 3");
  }
};

export function openWorkflow(container, processname) {
  getRootEl().pushDialog({
    type: "workflow",
    processname: processname,
    container: container
  });
}

window["openWorkflow"] = openWorkflow;

export function openDialog(dialogName) {
  getRootEl().pushDialog({
    type: dialogName
  });
}
