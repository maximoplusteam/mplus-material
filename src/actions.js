/*
Actions are the functions that can be called from anywhere on the page. They can be navigational (openPage,openDialog..), operating on the MboContainer (addRow, changeStatus, routeWorkflow), or something mobile specific (call number, open map, send sms...)

For the sake of simplicity, every action will have exactly two parameters - parameter object and the context (control from where the function was called). Parameter object will be destructured in the function call). Concrete action call will be defined in the designer. The application template has to implement the action call (every action has to be defined in this file)

Example:

{action name="openTab" param={tab:2}}

This will be defined in the designer, and the function will be 

openTab({tab:2})

Right now, every template will have the same list of actions, and the designer has to have the actions definition file (same like it has for the controls). Think how the template specific actions could be defined in the designer
*/

const getRootEl = () =>
  document.body.firstElementChild.shadowRoot.querySelector("mat-layout");
//this can be done easily for the web components. For the react and like, we need one level of indirection to get the component(maybe simple ref on the main component like {ref=>global["app"]=this})

export const actions = {
  openTab: ({ tab }) => {
    getRootEl().openTab(tab);
  }
};
