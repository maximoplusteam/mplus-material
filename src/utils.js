import { actions } from "./actions.js";

export function getRootEl() {
  return document.body.firstElementChild.shadowRoot.querySelector("mat-layout");
}

//shared list observers between modules

export let listOb;
//when animating, we need to stop displaying the changes from maximo, otherwise animation is choppy. Since it is not trivial to control this, we will introduce the animating propety on each top-level component, so when animating occurs this flyd stream is set

export function setListOb(_listOb) {
  listOb = _listOb;
}

export const FETCH_PERIOD = 200; //number of ms between next scro

export const invokeAction = (name, param, context) =>
  actions[name].call(context, param);
