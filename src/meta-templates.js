import { h } from "preact";
import { props, withComponent } from "skatejs";
import withPreact from "@skatejs/renderer-preact";

const Component = withComponent(withPreact());

import defineMuiComponents from "muicss-webcomp";
import fontawesome from "@fortawesome/fontawesome";
import faCheck from "@fortawesome/fontawesome-free-solid/faCheck";

export class TickIcon extends Component {
  render() {
    return (
      <div style="width:20px;height:20px;margin-right:7px;color:var(--primary-text-color)" />
    );
  }

  rendered() {
    let icon = fontawesome.icon(faCheck).html[0];
    this.shadowRoot.firstElementChild.innerHTML = icon;
  }
}

customElements.define("tick-icon", TickIcon);

class Paper extends Component {
  static get props() {
    return {
      noripple: props.boolean
    };
  }

  clickHandler = evt => {
    let rect = this.getBoundingClientRect();
    let x = evt.pageX - rect.left;
    let y = evt.pageY - rect.top;
    let _rippleEl = document.createElement("div");
    _rippleEl.innerHTML =
      "<div class='ripple-effect' style='top:" +
      y +
      "px;left:" +
      x +
      "px;'></div>";
    let rippleEl = _rippleEl.firstElementChild;

    this.rippleCont.appendChild(rippleEl);
    window.setTimeout(() => {
      this.rippleCont.removeChild(rippleEl);
    }, 480);
  };

  render() {
    let st = `
:host{
display:flex;
min-height:48px;
line-height:24px;
flex-direction: column;
justify-content: center;
flex: 1;
border-bottom: 1px;
border-bottom-style: solid;
border-bottom-color: #f5f5f5;
overflow:hidden;
}


.ripple-effect{
  position: absolute;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  background: grey;

    
  animation: ripple-animation 0.5s;
}


@keyframes ripple-animation {
    from {
      transform: scale(1);
      opacity: 0.4;
    }
    to {
      transform: scale(100);
      opacity: 0;
    }
}

.itemcont{
flex:1;
overflow:hidden;
position:relative;
display:flex;
align-items:center;
padding:10px;
}
`;
    if (this.noripple) {
      return (
        <div class="itemcont">
          <style>{st}</style>
          <slot />
        </div>
      );
    }
    return (
      <div
        onClick={this.clickHandler}
        ref={el => (this.rippleCont = el)}
        class="itemcont"
      >
        <style>{st}</style>
        <slot />
      </div>
    );
  }
}

customElements.define("paper-mat", Paper);

export const QbeWrapper = (selected, x) => {
  if ("Y" === selected) {
    return (
      <div style="display:flex;align-items:center">
        <tick-icon />
        <div style="flex:1">{x}</div>
      </div>
    );
  }
  return x;
};

export let Simple = (x, action) => <paper-mat onClick={action}>{x}</paper-mat>;

export let QbeSimple = (x, selected, action) => (
  <paper-mat onClick={action}>{QbeWrapper(selected, x)}</paper-mat>
);

//TODO every single template will have its qbe counterpart. Think about the generic way to introruce the qbe template from template
