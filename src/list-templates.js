import { h } from 'preact';
import * as template from './meta-templates.js';
import './meta-templates.js';
//temporary to test, remove it
/*This will be automatically genereated file by the designer
 There will be a list of pre-defined "meta-template" (template of templates). Each meta-template will have will have several attributes where the fields can be put. The designer will replace them and create the source files in list-templates.js. For example if this is the meta-template:

 (mainpart,datePart)=><div>
 <div>{mainPart></div>
 <div>{datePart}</div>
 <div>

 ,we can set the main part to "{x.PONUM},{x.DESCRIPTION}" and the tamplate will be genarated 
 (x)=>
 <div>
 <div>{x.PONUM},{x.DESCRIPTION}</div>
 <div>

 Idea! maybe it is better to define each meta-template as a component, than it become just the function call, no need to replace anything

 for example:
 (in metatemplate file)
 let Simple = (mainpart,datepart)=>
 <div>
 <div>{mainPart></div>
 <div>{datePart}</div>
 <div>;

 here then simple=(x)=>Simple(x.PONUM+","+x.DESCRIPTION, x.STATUSDATE);


 */

let listTemplates ={
    simplePO:({data:x, rowSelectedAction:action})=>{
	return template.Simple(`${x.PONUM} , ${x.DESCRIPTION}` , action);},
    valuelist:({data:x, rowSelectedAction:action})=>{
	return template.Simple(`${x.VALUE} , ${x.DESCRIPTION}` , action);},
    qbevaluelist:({data:x, rowSelectedAction:action})=>{
	return template.QbeSimple(`${x.VALUE} , ${x.DESCRIPTION}` ,
				  x["_SELECTED"], action);}//There should be a checkbox on the front end (or maybe iti is enough just to pass every time , but it will be ignored for non-qbe
    
};

export default listTemplates;
