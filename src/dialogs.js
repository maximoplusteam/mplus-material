import { h } from "preact";

export const dialogs = {
  random: (
    <div
      onClick={ev => console.log("focus!")}
      style={"background:" + getRandomColor()}
    />
  ),
  posatus: (
    <div>
      <rel-container
        id="pochangestatus"
        relationship="pochangestatus"
        container="pocont"
      />
      <max-section container="pochangestatus" columns={["stautus", "memo"]} />
      <mui-button onClick={ev => ref_cont.runMboCommand("execute")} />
    </div>
  )
};

export function getRandomColor() {
  let letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
