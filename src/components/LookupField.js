import { h } from "preact";
import { props } from "skatejs";
import { Component } from "mplus-webcomponents";

class LookupField extends Component {
  static get props() {
    return {
      label: props.string,
      value: props.string,
      disabled: props.boolean,
      enabled: props.boolean,
      errorText: props.string,
      showLookupF: props.object,
      listener: props.object,
      icon: props.string //actually icon component
    };
  }

  render({ props }) {
    let iconField = h(props.icon, {
      onclick: ev => {
        props.showLookupF(ev);
      },
      style: {
        position: "absolute",
        left: 0,
        top: 20,
        width: 20,
        height: 20,
        color: "var(--secondary-text-color)",
        zIndex: 1
      }
    });
    return (
      <div style="position:relative;display:inline-block;width:100%">
        <style>{":host{display:block}"}</style>
        {iconField}
        <mui-textfield
          label={props.label}
          value={props.value}
          disabled={!props.enabled}
          onChange={ev => props.listener(ev.target.value)}
          errorMessage={props.errorText}
          paddingLeft="25px"
        />
      </div>
    );
  }
}

customElements.define("lookup-field", LookupField);
