import { h } from "preact";
import { props } from "skatejs";
import { Component } from "mplus-webcomponents";
import style from "../css/mat-app-bar.css";

class MatAppBar extends Component {
  //Extension of the mui app bar, with pre-defined components: action buttom (menu or back button fom the dialogs, label, action buttons...

  static get props() {
    return {
      hasMenu: props.boolean,
      dialog: props.boolean,
      label: props.string,
      buttonAction: props.object,
      activeTab: props.number
    };
  }

  render({ props }) {
    let backIcon = (
      <svg
        fill="#000000"
        height="24"
        viewBox="0 0 24 24"
        width="24"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M0 0h24v24H0z" fill="none" />
        <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z" />
      </svg>
    );

    let menuIcon = (
      <svg
        fill="#000000"
        height="24"
        viewBox="0 0 24 24"
        width="24"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M0 0h24v24H0z" fill="none" />
        <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" />
      </svg>
    );
    let icon = props.hasMenu ? menuIcon : props.dialog ? backIcon : <div />;
    return (
      <div>
        <style>{style}</style>
        <app-bar>
          <div class="flexCont">
            <div class="menuIcon" onClick={props.buttonAction}>
              {icon}
            </div>
            <div class="appBarLabel">{props.label}</div>
          </div>
        </app-bar>
      </div>
    );
  }
}

customElements.define("mat-app-bar", MatAppBar);
