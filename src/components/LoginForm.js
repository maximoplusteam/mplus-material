import { h } from "preact";
import { props } from "skatejs";
import { Component } from "mplus-webcomponents";
import style from "../css/login-form.css";

class LoginForm extends Component {
  static get props() {
    return {
      open: props.boolean
    };
  }

  render({ props }) {
    return (
      <div>
        <style>{style}</style>
        <mui-overlay open={props.open}>
          <div
            class="login"
            ref={el => {
              this.loginEl = el;
            }}
          >
            <div class="login-fields">
              <mui-textfield id="username" label="Userame" />
              <mui-textfield id="password" label="Password" type="password" />
            </div>
            <div class="buttons">
              <mui-button onClick={ev => this.loginAction()} id="close-overlay">
                OK
              </mui-button>
            </div>
          </div>
        </mui-overlay>
      </div>
    );
  }

  rendered() {
    window.setTimeout(() => {
      let moveLeft = Math.trunc(this.loginEl.offsetWidth / 2);
      let moveUp = Math.trunc(this.loginEl.offsetHeight / 2);
      this.loginEl.style.marginTop = "-" + moveUp + "px";
      this.loginEl.style.marginLeft = "-" + moveLeft + "px";
    }, 0);
  }

  loginAction() {
    let userName = this.shadowRoot.getElementById("username").value;
    let password = this.shadowRoot.getElementById("password").value;
    maximoplus.core.max_login(
      userName,
      password,
      function(ok) {
        document.location.reload();
      },
      function(err) {
        console.log(err);
        maximoplus.core.handleErrorMessage("Invalid Username or Password");
      }
    );
  }
}

customElements.define("login-form", LoginForm);
