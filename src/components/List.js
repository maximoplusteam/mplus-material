import { h } from "preact";
import { List as WList } from "mplus-webcomponents";
import listTemplates from "../list-templates.js";
import { listOb, invokeAction, FETCH_PERIOD } from "../utils.js";
import { props } from "skatejs";

class List extends WList {
  //the list row woll be defined from template, so there is no need to define anything here. The things that will differ from original implementation
  //infinite list needs to be implemented. Filter should be displaud as a fab button  The question is should fab button be part of th elist itself, or should it be the part of the root component (same as dialogs, so all the components can put or remove buttons)

  constructor() {
    super();
    this.state.lastTimeFetched = 0;
  }

  static get props() {
    return {
      ...super.props,
      chooseAction: props.object
    };
  }
  getFilterButton() {
    return (
      <mui-button fab danger onClick={ev => this.showFilter()}>
        <filter-icon />
      </mui-button>
    );
  }

  drawList(rows, button) {
    //TODO override this, I think this is the place where the infinite scrolling should be implemented.
    //Maybe it is a good idea to put the variants - infinite list vs load manually (using the buttons)
    return (
      <div>
        <style>{":host{display:block}"}</style>
        {rows}
      </div>
    );
    //host display block so the interception observer can detect it properly
    //TODO put button (fab)
  }

  getListTemplate(templateId) {
    return listTemplates[templateId];
  }

  notifyEndOfPage() {
    if (Date.now() - this.state.lastTimeFetched > FETCH_PERIOD) {
      this.setState({ lastTimeFetched: Date.now() });
      setTimeout(() => {
        this.mp.fetchMore(20);
      }, 0); //experiment
    }
  }

  connected() {
    super.connected();
    listOb.observe(this);
  }

  putContainer(mboCont) {
    if (this.chooseAction && Object.keys(this.props.chooseAction).length > 0) {
      this.selectableF = () =>
        invokeAction(
          this.props.chooseAction.name,
          this.props.chooseAction.param,
          this
        );
    }
    super.putContainer(mboCont);
  }
}

customElements.define("max-list", List);
