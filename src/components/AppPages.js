import { h } from "preact";
import { animating, Component } from "mplus-webcomponents";
import style from "../css/app-pages.css";
import { props } from "skatejs";

class AppPages extends Component {
  state = { prevActive: -1, transitioning: false };

  static get props() {
    return {
      active: props.number
    };
  }
  render({ props, state }) {
    return (
      <div class="rt">
        <style>{style}</style>
        <slot />
      </div>
    );
  }

  rendered() {
    if (this.state.prevActive && this.state.prevActive == this.props.active) {
      return;
    }
    // animating(true);
    this.setState({ prevActive: this.props.active });

    let children = this.children;
    if (this.children.length == 1 && this.children[0].tagName == "SLOT") {
      children = this.children[0].assignedNodes();
      //if the content of the slot from the parent element is passed directly, the slot element is passed! ( I didn't expect it)
    }

    if (!this.listenerInitialized) {
      this.listenerInitialized = true;
      this.addEventListener("transitionstart", ev => {
        animating(true);
      });

      this.addEventListener("transitionend", ev => {
        for (let j = 0; j < children.length; j++) {
          if (j == this.props.active) {
            children[j].style.display = "block";
          } else {
            children[j].style.display = "none";
          }
        }
        animating(false);
      });
    }

    animating(true);

    for (let j = 0; j < children.length; j++) {
      let trPerc = (j - this.props.active) * 100;
      let calcTr = "translate3d(" + trPerc.toString() + "%,0,0)";
      window.setTimeout(() => {
        children[j].style.transform = calcTr;
      }, 50);
      //if it is done at the  same time, transitioning will be done from display:none, and would not work properly
      children[j].style.display = "block";
    }
  }
}

customElements.define("app-pages", AppPages);
