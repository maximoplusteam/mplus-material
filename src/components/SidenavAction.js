import { h } from "preact";
import { props } from "skatejs";
import { Component } from "mplus-webcomponents";
import style from "../css/side-nav-action.css";

class SideNavAction extends Component {
  static get props() {
    return {
      action: props.object
    };
  }

  render({ props }) {
    return (
      <div>
        <style>{style}</style>
        <mui-ripple
          onClick={ev => {
            this.handleClick(ev);
          }}
        >
          <div id="actiondiv">{props.action.label}</div>
        </mui-ripple>
      </div>
    );
  }

  handleClick(ev) {
    const action = this.props.action.action;
    if (typeof action == "function") {
      action.call(ev);
    } else {
      window["actions"][action].call(ev);
    }
  }
}

customElements.define("side-nav-action", SideNavAction);
