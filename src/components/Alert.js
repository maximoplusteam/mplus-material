import { h } from "preact";
import { Component } from "mplus-webcomponents";
import { props } from "skatejs";
import style from "../css/alert.css";

class Alert extends Component {
  state = { open: false };
  static get props() {
    return {
      title: props.string,
      message: props.string
    };
  }

  render({ props, state }) {
    return (
      <div>
        <style>{style}</style>
        <mui-overlay open={state.open}>
          <div
            class="alert"
            ref={el => {
              this.alertEl = el;
            }}
          >
            <div class="title">
              <mui-text text-style="headline">{props.title}</mui-text>
            </div>
            <div class="body">
              <mui-text text-style="body2">{props.message}</mui-text>
            </div>
          </div>
          <mui-button
            primary
            onClick={ev => {
              this.setState({ open: false });
            }}
          >
            OK
          </mui-button>
        </mui-overlay>
      </div>
    );
  }

  open() {
    this.setState({ open: true });
  }

  rendered() {
    window.setTimeout(() => {
      let moveLeft = Math.trunc(this.alertEl.offsetWidth / 2);
      let moveUp = Math.trunc(this.alertEl.offsetHeight / 2);
      this.alertEl.style.marginTop = "-" + moveUp + "px";
      this.alertEl.style.marginLeft = "-" + moveLeft + "px";
    }, 0);
  }
}

customElements.define("maximo-alert", Alert);
