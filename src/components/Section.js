import { h } from "preact";
import {
  Section as WSection,
  QbeSection as WQbeSection
} from "mplus-webcomponents";
import { invokeAction } from "../utils.js";

class Section extends WSection {
  drawFields(fields) {
    return <mui-panel>{fields}</mui-panel>;
  }
}

customElements.define("max-section", Section);

class QbeSection extends WQbeSection {
  drawFields(fields, buttons) {
    return (
      <mui-panel>
        {fields}
        {buttons}
      </mui-panel>
    );
  }

  renderSearchButtons(buttons) {
    let rbs = buttons.map((button, i) => {
      let primary = i == 0 ? { primary: true } : {};
      let f =
        this.afterSearch &&
        Object.keys(this.afterSearch).length > 0 &&
        button.label === "Search"
          ? () => {
              invokeAction(this.afterSearch.name, this.afterSearch.param, this);
              button.action.call(this);
            }
          : button.action;
      return (
        <mui-button {...primary} onClick={f}>
          {button.label}
        </mui-button>
      );
    });
    return <div>{rbs}</div>;
  }
}

customElements.define("qbe-section", QbeSection);
