import { h } from "preact";
import { RadioButton as WRadioButton } from "mplus-webcomponents";

export class RadioButton extends WRadioButton {
  render({ props, state }) {
    let drs = [];
    const pickerkeycol = props.pickerkeycol.toUpperCase();
    const pickercol = props.pickercol.toUpperCase();
    const lsnr = props.changeListener;
    if (state.maxrows) {
      drs = state.maxrows.map(function(object, i) {
        let ticked = object.picked ? <tick-icon /> : <div />;
        let optionKey = object.data[pickerkeycol];
        let optionVal = object.data[pickercol];

        return (
          <paper-mat onClick={ev => lsnr(optionKey)}>
            {ticked}
            {optionVal}
          </paper-mat>
        );
      });
    }
    return <div>{drs}</div>;
  }
}

customElements.define("max-radio-group", RadioButton);
