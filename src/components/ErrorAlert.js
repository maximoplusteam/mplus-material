import { h } from "preact";
import { props } from "skatejs";
import { Component } from "mplus-webcomponents";

export class ErrorAlert extends Component {
  static get props() {
    return {
      open: props.boolean,
      errorText: props.string,
      closeDialog: props.object //move up
    };
  }
  constructor() {
    super();
    this.state.open = this.props.open;
  }
  render({ props, state }) {
    return (
      <mui-overlay animated open={props.open}>
        <div style="height:100%;width:100%;display:flex;flex-direction:row;justify-content:center;align-content:center">
          <div style="min-width:300px;max-width:80%;display:flex;flex-direction:column;background:white;margin:auto;padding:10px; box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);">
            <div style="flex:3;font-size:18px;padding:24px">
              {props.errorText}
            </div>
            <div style="flex:initial;display:flex;justify-content:flex-end">
              <mui-button
                onClick={ev => props.closeDialog()}
                id="close-overlay"
              >
                OK
              </mui-button>
            </div>
          </div>
        </div>
      </mui-overlay>
    );
  }
}

customElements.define("error-alert", ErrorAlert);
