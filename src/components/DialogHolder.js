import { h } from "preact";
import { animating, Component } from "mplus-webcomponents";
import { getRootEl } from "../utils.js";
import style from "../css/dialog-holder.css";

class DialogHolder extends Component {
  state = { dialogs: [], animated: false };

  render({ props, state }) {
    let startZIndex = 2;
    let dialogMap = getRootEl().getDialogMap();
    let dR = state.dialogs.map((d, index) => {
      let lab = () => {
        switch (d.type) {
          case "qbelist":
            return "Pick one or more values";
          case "list":
            return "Pick a value";
          default:
            return d.label ? d.label : "Dialog";
        }
      };
      return (
        <div class="dialogWrapper" style={"z-index:" + startZIndex + index}>
          <div>
            <mat-app-bar
              dialog
              label={lab()}
              buttonAction={ev =>
                d.closeAction ? d.closeAction() : this.removeDialog()
              }
            />
            <div class="contentCont">{dialogMap[d.type](d)}</div>
          </div>
        </div>
      );
    });
    return (
      <div>
        <style>{style}</style>
        <div id="dialogs">{dR}</div>
      </div>
    );
  }

  rendered() {
    //to enable the animation of the newly added elements, add the class animated to the elements
    let els = this.shadowRoot.getElementById("dialogs");

    for (let j = 0; j < els.children.length; j++) {
      let el = els.children[j];

      window.setTimeout(() => {
        animating(true);
        el.classList.add("animated");
        let f = ev => {
          el.removeEventListener("transitionend", f);
          animating(false);
        };
        el.addEventListener("transitionend", f);
      }, 20);
    }
  }

  removeDialog() {
    //first remove animation
    let children = this.shadowRoot.querySelector("#dialogs").children;
    if (children) {
      let el = children[children.length - 1];
      el.classList.remove("animated");
      animating(true);
      let f = ev => {
        el.removeEventListener("transitionend", f);
        const newDials = this.state.dialogs.slice();
        newDials.pop();
        this.setState({ dialogs: newDials });
        animating(false);
        getRootEl().unstackFabs();
      };
      el.addEventListener("transitionend", f);
    }
  }

  pushDialog(dialogMap, d) {
    //    let dialEl = dialogMap[d.type](d);
    const newDials = this.state.dialogs.slice();
    newDials.push(d);
    this.setState({ dialogs: newDials });
    getRootEl().stackFabs();
    if (d.closeAction) {
      getRootEl().pushFab({ icon: faCheck, callback: d.closeAction });
    }
    if (d.filterAction) {
      getRootEl().pushFab({ icon: faFilter, callback: d.filterAction });
    }
  }
}

customElements.define("mat-dialogs", DialogHolder);
