import { h } from "preact";
import fontawesome from "@fortawesome/fontawesome";
import faSearch from "@fortawesome/fontawesome-free-solid/faSearch";
import faCalendar from "@fortawesome/fontawesome-free-regular/faCalendarAlt";
import faFilter from "@fortawesome/fontawesome-free-solid/faFilter";
import faCheck from "@fortawesome/fontawesome-free-solid/faCheck";
import faUndo from "@fortawesome/fontawesome-free-solid/faUndo";
import faClose from "@fortawesome/fontawesome-free-solid/faTimes";
import { Component } from "mplus-webcomponents";
import { props } from "skatejs";
import style from "../css/icons.css";

const checkIcon = fontawesome.icon(faCheck);

export class LookupIcon extends Component {
  render() {
    return <div />;
  }
  rendered() {
    let searchIcon = fontawesome.icon(faSearch).html[0];
    this.shadowRoot.firstElementChild.innerHTML = searchIcon;
  }
}

customElements.define("lookup-icon", LookupIcon);

export class FaIcon extends Component {
  static get props() {
    return {
      icon: props.object
    };
  }
  render() {
    return (
      <div style="display:flex">
        <style>{style}</style>
        <div id="ic" />
      </div>
    );
  }

  rendered() {
    let ic = fontawesome.icon(this.props.icon).html[0];
    this.shadowRoot.getElementById("ic").innerHTML = ic;
  }
}

customElements.define("fa-icon", FaIcon);

export class CalendarIcon extends Component {
  render() {
    return <div />;
  }
  rendered() {
    let dateIcon = fontawesome.icon(faCalendar).html[0];
    this.shadowRoot.firstElementChild.innerHTML = dateIcon;
  }
}

customElements.define("calendar-icon", CalendarIcon);

export class FilterIcon extends Component {
  render() {
    return <div />;
  }
  rendered() {
    let filterIcon = fontawesome.icon(faCalendar).html[0];
    this.shadowRoot.firstElementChild.innerHTML = filterIcon;
  }
}

customElements.define("filter-icon", FilterIcon);
