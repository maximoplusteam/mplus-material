import { h } from "preact";
import faCheck from "@fortawesome/fontawesome-free-solid/faCheck";
import { props } from "skatejs";
import { dialogs } from "../dialogs.js";
import { dialogs as Wdialogs, Component } from "mplus-webcomponents";
import { listOb, setListOb, invokeAction } from "../utils.js";
import themes from "../css/theme-default.css";
import sidenavstyles from "sidenavjs/dist/sidenav.css";
import style from "../css/mat-layout.css";

const Sidenav = require("sidenavjs/dist/sidenav.min.js");

const fabs = _fabs => {
  if (!_fabs) return null;
  let _ff = _fabs.map((f, i) => {
    let pp = i == 0 ? { danger: true } : i == 1 ? { primary: true } : {};
    return (
      <mui-button
        {...pp}
        fab
        style="display:block"
        onClick={() =>
          f.callback
            ? f.callback()
            : invokeAction(f.action.name, f.action.param, this)
        }
      >
        <fa-icon icon={f.icon} />
      </mui-button>
    );
  });
  return (
    <div style="position:fixed;bottom:5px;right:20px;z-index:22;">{_ff}</div>
  );
};

class MatLayout extends Component {
  //for this layout, we will have tabs, and the scrolling is done  on the layoyt level. We need to notify the active list when the user has scrolled to the bottom of the page, so it can fetch more in case of infininte lists or something similar
  state = {
    errorText: "",
    errorOpen: false,
    needsLogin: false,
    transitioning: false,
    activeList: null
  };
  constructor() {
    super();
    //when the list enters the viewport activate it (so we scroll on the top level and call just the active list fetch)
    this.closeErrorDialog = this.closeErrorDialog.bind(this);
    setListOb(
      new IntersectionObserver(
        ev => {
          for (let evj of ev) {
            if (evj.isIntersecting == false) {
              this.setActiveList(null);
            } else {
              this.setActiveList(evj.target);
              break;
            }
          }
        },
        { threshold: 0.1 }
      )
    );
  }

  closeErrorDialog() {
    this.setState({ errorText: "", errorOpen: false });
  }
  static get props() {
    return {
      tabs: props.array,
      headerLabel: props.string,
      sideNavActions: props.array
    };
  }

  connected() {
    maximoplus.core.globalFunctions.global_login_function = err => {
      this.setState({ needsLogin: true });
    };
  }

  openErrorDialog(errorText) {
    this.setState({ errorText: errorText, errorOpen: true });
  }

  closeErrorDialog() {
    this.setState({ errorText: "", errorOpen: false });
  }

  getDialogMap() {
    //webcomp will define the basic dialogs that can remain the same across all the implementations
    //dialog.js will be the holder of custom dialogs created by the visual designer
    return Object.assign({}, Wdialogs, dialogs);
  }

  pushDialog(d) {
    const dialogComp = this.shadowRoot.firstElementChild.getElementsByTagName(
      "MAT-DIALOGS"
    )[0];
    dialogComp.pushDialog(this.getDialogMap(), d);
  }

  removeDialog() {
    const dialogComp = this.shadowRoot.firstElementChild.getElementsByTagName(
      "MAT-DIALOGS"
    )[0];
    dialogComp.removeDialog();
  }

  closeDialog() {
    this.removeDialog(); //synonym
  }

  render({ props, state }) {
    const tabs =
      props.tabs && props.tabs.length != 0 ? (
        <tab-bar tabs={props.tabs} justified active={state.activeTab} />
      ) : (
        <div />
      );
    const sideNavs = props.sideNavActions.map(s => (
      <side-nav-action action={s} />
    ));
    return (
      <div>
        <style>{themes + style + sidenavstyles}</style>

        <div class="app-container">
          <div id="backdrop" />
          <div id="side-nav">{sideNavs}</div>
          <div id="app-main">
            <div class="content">
              <div id="myheader">
                <mat-app-bar
                  buttonAction={ev => this.sideNavOpen()}
                  label={props.headerLabel}
                  hasMenu
                />
                {tabs}
              </div>
              <app-pages active={state.activePage}>
                <slot />
              </app-pages>
            </div>
          </div>
          <error-alert
            errorText={state.errorText}
            open={state.errorOpen}
            closeDialog={this.closeErrorDialog}
          />
          <mat-dialogs />
          <login-form open={state.needsLogin} />
          {fabs(state.fabs)}
        </div>
      </div>
    );
  }

  openPage(pageNum) {
    this.setState({ activePage: pageNum });
  }

  openTab(tabNum) {
    this.setState({ activePage: tabNum, activeTab: tabNum });
  }

  pushFab({ icon, action, callback }) {
    setTimeout(() => {
      this.setState({
        fabs: [
          { icon: icon, action: action, callback: callback },
          ...this.state.fabs
        ]
      });
    }, 200);
    //delay accoring to Material design spec (show delay in displaying fab when moving between pages
  }

  popFab() {
    this.setState({ fabs: this.state.fabs.slice(1) });
  }

  stackFabs() {
    this.setState({
      fabStack: [this.state.fabs, ...this.state.fabStack],
      fabs: []
    });
  }

  unstackFabs() {
    let fs = this.state.fabStack[0];
    if (fs) {
      this.setState({ fabs: fs, fabStack: this.state.fabStack.slice(1) });
    }
  }

  pushTestFab() {
    this.pushFab(faCheck, { name: "openTab", param: { tab: 2 } });
  }

  sideNavOpen() {
    this.sd.open();
  }

  sideNavClose() {
    this.sd.close();
  }

  rendered() {
    this.addEventListener("animationstart", ev => {
      console.log("animation starts");
    });

    this.addEventListener("animationend", ev => {
      console.log("animation ends");
    });

    this.sd = new Sidenav({
      content: this.shadowRoot.getElementById("app-main"),
      sidenav: this.shadowRoot.getElementById("side-nav"),
      backdrop: this.shadowRoot.getElementById("backdrop")
    });
    let header = this.shadowRoot.querySelector("#myheader");

    let previousScrollTop = 0;
    let minimumSignificantMove = 10; //if the move is less than this, don't do anything
    let inside = false;
    var that = this;
    if (!this.listenerInitialized) {
      this.listenerInitialized = true;
      this.addEventListener("paneChange", ev => {
        this.setState({
          activeTab: this.shadowRoot.querySelector("tab-bar").active,
          transitioning: true
        });
        let evv = new Event("tabChange", { bubbles: true, composed: true });
        evv.activeTab = this.state.activeTab;
        this.dispatchEvent(evv);
        this.openPage(this.state.activeTab);
      });
      this.addEventListener("pageOpened", ev => {
        this.setState({ transitioning: false });
      });

      //	    let e2l = this.shadowRoot.querySelector("#app-container");
      //	    e2l.addEventListener("transitionstart",(ev)=>{console.log("Side nav start");});
      //	    e2l.addEventListener("transitionend",(ev)=>{console.log("Side nav stop");});
    }
    function periodicScrollCheck() {
      inside = true;
      //	    let el = document.scrollingElement;//todo change this so can be another element
      let el = that;
      var header = that.shadowRoot.querySelector("#myheader");
      let headerHeight = header.getBoundingClientRect().height;

      if (el.scrollTop == 0) {
        //header is in the view port - remove the fixed position
        header.classList.remove("hh");
        header.style.top = "";
        inside = false;
        return;
      }

      if (Math.abs(el.scrollTop - previousScrollTop) < minimumSignificantMove) {
        inside = false;
        return;
      }

      header.classList.add("hh");

      if (previousScrollTop < el.scrollTop) {
        let hs = "-" + headerHeight.toString() + "px";
        header.style.top = hs;
      } else {
        header.style.top = "0px";
      }
      previousScrollTop = el.scrollTop;
      inside = false;

      if (
        Math.abs(el.scrollHeight - (el.scrollTop + el.clientHeight)) <
        minimumSignificantMove
      ) {
        if (that.state.activeList) {
          that.state.activeList.notifyEndOfPage();
        }
      }
    }

    this.addEventListener("scroll", function(ev) {
      if (!inside) {
        requestAnimationFrame(periodicScrollCheck);
      }
    });

    let observer = new MutationObserver(function(event) {
      /*
	     This is a hack to adapt the Sidenav library to work with the web components. By default library puts the class sn-visible on hmtl once the side nav is active, and removes it once is closed
	     The selectors that work normally will not work for shadow dom, and we use the mutation observer to copy the classes
	     */
      if (event[0].target.classList.contains("sn-visible")) {
        that.shadowRoot
          .querySelector(".app-container")
          .classList.add("sn-visible");
      } else {
        that.shadowRoot
          .querySelector(".app-container")
          .classList.remove("sn-visible");
      }
    });

    observer.observe(document.getRootNode().firstElementChild, {
      attributes: true,
      attributeFilter: ["class"],
      childList: false,
      characterData: false
    });
  }

  setActiveList(list) {
    this.setState({ activeList: list });
  }
}

customElements.define("mat-layout", MatLayout);
