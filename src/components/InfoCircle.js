import { h } from "preact";
import { Component } from "mplus-webcomponents";
import { props } from "skatejs";
import * as colors from "material-colors";

function getRandomLetterColor(colorKey) {
  var color = colors[colorKey]["a700"] || colors[colorKey]["900"];

  // Invalid color saturation value?
  if (color == undefined) {
    // No saturation for black or white,
    // so return the next random color instead
    return getRandomLetterColor(colorKey);
  }

  // Return current color hex
  return color;
}

class InfoCircle extends Component {
  static get props() {
    return {
      title: props.string
    };
  }
  render({ props }) {
    let colorKeys = Object.keys(colors);
    let color = getRandomLetterColor(
      colorKeys[Math.floor(Math.random() * (colorKeys.length - 7))]
    );
    let letter = props.title.toUpperCase()[0];

    const style = `
#circle{
    border-radius:50%;
    background:${color};
    height:var(--circle-size,35px);
    width:var(--circle-size,35px);
    line-height:var(--circle-size,35px);
    font-size: calc(var(--circle-size,35px) - 15px);
    text-align:center;
    margin:3px;
    color:white;
}`;

    return (
      <div>
        <style>{style}</style>
        <div id="circle">{letter}</div>
      </div>
    );
  }
}

customElements.define("info-circle", InfoCircle);
