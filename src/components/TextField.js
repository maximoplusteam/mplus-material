import { h } from "preact";
import { TextField as WTextField } from "mplus-webcomponents";

class TextField extends WTextField {
  render({ props }) {
    if ("YORN" == props.type && !props.qbe) {
      let _props = {
        disabled: !props.enabled,
        onChange: ev => props.listener(ev.target.value)
      };
      if (props.value == "Y") _props.checked = true;
      return <mui-checkbox {..._props} />;
    }
    let _icon =
      "DATE" == this.type || "DATETIME" == this.type
        ? "calendar-icon"
        : "lookup-icon";
    if (props.showLookupF && typeof props.showLookupF == "function") {
      return (
        <lookup-field
          showLookupF={props.showLookupF}
          icon={_icon}
          label={props.label}
          value={props.value}
          enabled={props.enabled}
          listener={props.listener}
          errorMessage={props.errorText}
        />
      );
    }
    return (
      <div>
        <style>{":host{display:block}"}</style>
        <mui-textfield
          label={props.label}
          value={props.value}
          disabled={!props.enabled}
          onChange={ev => props.listener(ev.target.value)}
          errorMessage={props.errorText}
        />
      </div>
    );
  }
}

customElements.define("text-field", TextField);
