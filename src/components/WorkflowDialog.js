import { h } from "preact";
import { WorkflowDialog as WWorkflowDialog } from "mplus-webcomponents";
import { getRootEl } from "../utils.js";

class WorkflowDialog extends WWorkflowDialog {
  drawWorkflow(title, section, buttons) {
    const btns = buttons.map((b, i) => {
      const primary = i === 0 ? { primary: true } : {};
      return (
        <mui-button {...primary} onClick={b.actionFunction}>
          {b.label}
        </mui-button>
      );
    });
    return (
      <div style="padding:10px;">
        <div style="font-size:20px;padding:20px;">{title}</div>
        {section}
        {btns}
      </div>
    );
  }

  closeDialog() {
    getRootEl().closeDialog();
  }
}

customElements.define("max-workflow", WorkflowDialog);
