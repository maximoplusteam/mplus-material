import "@webcomponents/webcomponentsjs/custom-elements-es5-adapter.js";
import { props, withComponent } from "skatejs";
import withPreact from "@skatejs/renderer-preact";
import { h } from "preact";
import "typeface-roboto";
import "./components.js";

const Component = withComponent(withPreact());

class SimpleApp extends Component {
  logit = () => {
    console.log("bla");
  };

  render() {
    let tabs = ["Search", "POs", "Details"];

    return (
      <div>
        <mat-layout
          tabs={tabs}
          headerLabel="App Template"
          sideNavActions={[
            {
              label: "Action 1",
              action: ev => {
                alert("Action 1");
              }
            },
            {
              label: "Action 2",
              action: ev => {
                console.log("Action 2");
              }
            },
            {
              label: "Action 3",
              action: "action3"
            }
          ]}
        >
          <div>
            <qbe-section
              container="pocont"
              columns={[
                "ponum",
                "description",
                "status",
                "vendor",
                "shipvia",
                "totalcost"
              ]}
              afterSearch={{
                name: "openTab",
                param: { tab: 1 }
              }}
              metadata={{
                SHIPVIA: {
                  hasLookup: true,
                  listTemplate: "qbevaluelist",
                  filterTemplate: "valuelist"
                },
                STATUS: {
                  hasLookup: true,
                  listTemplate: "qbevaluelist",
                  filterTemplate: "valuelist"
                }
              }}
            />
          </div>
          <div>
            <max-list
              columns={["ponum", "description"]}
              norows="20"
              initdata="true"
              list-template="simplePO"
              container="pocont"
              chooseAction={{ name: "openTab", param: { tab: 2 } }}
            />
          </div>
          <div>
            <max-section
              container="pocont"
              columns={[
                "ponum",
                "description",
                "status",
                "vendor",
                "shipvia",
                "totalcost"
              ]}
              metadata={{
                SHIPVIA: {
                  hasLookup: true,
                  listTemplate: "valuelist",
                  filterTemplate: "valuelist"
                }
              }}
            />
          </div>
        </mat-layout>
        {/*<app-container mboname="postd" appname="po" id="pocont" wfprocess="pomain"></app-container>*/}
        <app-container
          mboname="po"
          appname="po"
          id="pocont"
          wfprocess="pomain"
          offlineenabled="true"
        />
      </div>
    );
  }
}

customElements.define("simple-app", SimpleApp);
